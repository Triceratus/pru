---
title: Plantilla Beamer
author:
	- Camilo Caparrós Laiz
fonttheme: structurebold
colortheme: rose
navigation: horizontal
---

# MAtemáticas

## Estadística

$$\bar{x} = \sum_{i=1}^{n} (x_i)$$

. . .

## Cálculo Lambda

$$(\lambda x.x)$$

. . .

## Cálculo de predicados

$$\forall x: P(x) \wedge x \in \mathbb{Z}$$

---

Continuando con lo anterior.

1. A
2. B
3. C
4. D
5. E
6. F

> ¡Hasta luego pelucas!

# Imagen

En esta diapositiva se ve una imagen.

:::::: {.columns}
::: {.column}
![Trash](img/imagen.png)
:::
::: {.column}
##

Cuerpo.

## Conclusión

- Teorema 1.
- Teorema 2.
:::
::::::
