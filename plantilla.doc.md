---
lang: es-ES
numbersections: true
header-includes:
	\newcommand{\hideFromPandoc}[1]{#1}
	\hideFromPandoc{
		\let\Begin\begin
		\let\End\end
	}

	\usepackage[a4paper, margin=1in]{geometry}
	\setlength{\parskip}{1em}
	
	\usepackage{fontspec}
	\setmainfont[Scale = 1.0]{Source Sans Pro}

	\pagestyle{plain}
	
	\usepackage{fancyvrb}
	\usepackage{multicol}
---

\title{ {\Huge \textbf{Plantilla documentación}} }
\author{\textbf{Nombre Apellido Apellido} \\ \texttt{correo@web.dom}
\and \textbf{Nombre Apellido Apellido} \\ \texttt{correo@web.dom}}
\date{ \textbf{Grupo 1} \\ Abril 1999}
\maketitle
\newpage

\tableofcontents
\newpage

# Sección

Texto normal *cursiva* **negrita** ~~tachado~~ 2^10^ 2~1~ \*\*Texto escapado\*\*.

## Subsección

Texto normal de subsección.

### Subsubsección

Texto normal de subsubsección.

#### Párrafo

Texto normal de párrafo.

##### Subpárrafo

Texto normal de subpárrafo.

# Listas

Sin orden:

- Elemento.
- Elemento.
- Elemento.

---

Con orden:

1. Elemento.
2. Elemento.
3. Elemento.

# Tablas

| **Actividad** | **Frecuencia** | **Tiempo** |
| :------------ | :------------- | ---------: |
| Dormir | L, M, X, J, V, S, D | 9h |
| Transporte | L, M, X, J | 3h |
| Universidad | L, M, X, J | 5h |
| Estudiar | M, J, V, S, D | 4h |

# Fórmulas matemáticas

$$\bar{x} = \sum_{i = 1}^{n} (x_{i})$$

$$\forall x: P(x) \wedge x \in \mathbb{Z}$$

# Código

Código **_inline_**: la variable `l` es de tipo `List<String>`.

Código escrito en el **documento directamente**:

```C
int main(int argc, char ** argv) {
	return 0;
}
```

Código escrito en **fichero**:

\VerbatimInput{code/main.c}

Código escrito en **fichero**, pero especifica **rango**:

\VerbatimInput[firstline=1, lastline=2]{code/main.c}

# Imágenes

![Papelera](img/imagen.png)

# Columnas

\Begin{multicols}{2}

**Paso por valor**

```C
void f(int x) {
	x = 4;
}

...

x = 1;
f(x);
/* x = 4 */
```

**Paso por referencia**

```C
void f(int * x) {
	*x = 4;
}

...

x = 1;
f(x);
/* x = 4 */
```

\End{multicols}

# Texto citado

Esta oración tiene una nota[^nota]. El siguiente texto [\ref{chapuza}] está citado.

[^nota]: No hay nota.

# Referencias

\label{chapuza} [\ref{chapuza}] Autor, Título. Extras. Chapuza. [www.url.com](www.url.com)
