# SOURCES
SRCS = $(wildcard *.md)

# OBJECTS
OBJS = $(patsubst %.md,%.pdf,$(SRCS))

# RULES
all: $(OBJS)

%.doc.pdf: %.doc.md
	pandoc $< -o $@ --pdf-engine=xelatex --highlight-style=tango

%.beamer.pdf: %.beamer.md
	pandoc $< -t beamer -o $@ --pdf-engine=xelatex
